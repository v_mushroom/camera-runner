/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner;

import com.mush.camerarunner.camera.CameraConnection;
import com.mush.camerarunner.camera.CameraStatusOverlay;
import com.mush.camerarunner.camera.PlayerTracker;
import com.mush.camerarunner.stages.CalibrationStage;
import com.mush.camerarunner.stages.CalibrationView;
import com.mush.camerarunner.stages.HomeStage;
import com.mush.camerarunner.stages.PlayStage;
import com.mush.camerarunner.stages.Stage;
import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.GameKeyboard;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.render.DelegatingGameRenderer;
import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.render.Renderable;
import com.mush.game.utils.swing.Game;
import com.mush.game.utils.swing.GameKeyListener;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class CameraRunner implements Updateable, Renderable {

    public enum StageState {
        HOME,
        CALIBRATION,
        PLAY,
        OVER,
        QUIT
    }

    public static class ChangeStageEvent {

        public final StageState state;

        public ChangeStageEvent(StageState state) {
            this.state = state;
        }

    }

    private final DelegatingGameRenderer renderer;
    private final Game engine;
    private final GameKeyListener keyListener;
    private CameraConnection cameraConnection;
    private PlayerTracker playerTracker;
    private BufferedImage cameraImage;
    private CameraStatusOverlay cameraStatusRenderer;
    private Stage stage;
    private boolean fullScreen = false;

    private int renderXOffset = 0;
    private int renderYOffset = 0;

    public CameraRunner() {
        renderer = new DelegatingGameRenderer(Config.VIEW_WIDTH, Config.VIEW_HEIGHT);
        keyListener = new GameKeyListener();
        engine = new Game(this, renderer, keyListener);
    }

    public void setup() {
        engine.frameTitle = "CameraRunner";
        engine.preferredSize.setSize(Config.VIEW_WIDTH, Config.VIEW_HEIGHT);
        engine.setPauseOnLoseFocus(true);
        engine.setFrameUndecorated(true);
        engine.frameIcon = "res/icon.png";
        engine.start();

        renderer.setRenderable(this);
        renderer.showFps(true);
        resizeToNormal();

        GameEventQueue.category(CameraConnection.QUEUE).addEventListener(this);

        cameraConnection = new CameraConnection();
        cameraConnection.setup();
        playerTracker = new PlayerTracker();
        cameraStatusRenderer = new CameraStatusOverlay(cameraConnection, playerTracker);

        GameEventQueue.category(GameEventQueue.Categories.NAVIGATION).addEventListener(this);

        GameKeyboard gameKeyboard = new GameKeyboard(GameEventQueue.Categories.SYSTEM);
        gameKeyboard.bindActionKey(KeyEvent.VK_F4, StageState.QUIT);
        gameKeyboard.bindActionKey(KeyEvent.VK_HOME, KeyEvent.VK_HOME);
        keyListener.addKeyboard(gameKeyboard);

        GameEventQueue.category(GameEventQueue.Categories.SYSTEM).addEventListener(this);

        changeStage(StageState.HOME);
    }

    private void resizeToNormal() {
        Logger.getGlobal().log(Level.INFO, "Resizing to normal");

        renderer.setPixelScale(Config.PIXEL_SIZE);
        engine.resize(Config.VIEW_WIDTH, Config.VIEW_HEIGHT);
        renderXOffset = 0;
        renderYOffset = 0;
    }

    private void resizeToFullScreen() {
        Logger.getGlobal().log(Level.INFO, "Resizing to full screen");

        Dimension display = engine.getDisplaySize(null);
        double wScale = (double) display.width / Config.VIEW_WIDTH;
        double hScale = (double) display.height / Config.VIEW_HEIGHT;
        double scale = Math.min(wScale, hScale);
        renderer.setPixelScale(Config.PIXEL_SIZE * scale);
        engine.resize(display.width, display.height);

        renderXOffset = (renderer.getScreenImage().getWidth() - Config.getPixelWidth()) / 2;
        renderYOffset = (renderer.getScreenImage().getHeight() - Config.getPixelHeight()) / 2;
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof StageState) {
            switch ((StageState) action.message) {
                case QUIT:
                    engine.close();
                    break;
            }
        } else if (action.message instanceof Integer) {
            switch ((Integer) action.message) {
                case KeyEvent.VK_HOME:
                    toggleFullScreen();
                    break;
            }
        }
    }

    @OnGameEvent
    public void onCameraEvent(CameraConnection.ImageGrabbedEvent event) {
        cameraImage = event.image;
        playerTracker.process(cameraImage);
    }

    @OnGameEvent
    public void onChangeStateEvent(ChangeStageEvent event) {
        changeStage(event.state);
    }

    @Override
    public void update(double elapsedSeconds) {
        try {
            if (stage != null) {
                stage.update(elapsedSeconds);
            }

            GameEventQueue.processQueue();
        } catch (Exception e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage());
        }
    }

    @Override
    public void updateCurrentFps(double fps) {

    }

    public void changeStage(StageState state) {
        Logger.getGlobal().log(Level.INFO, "Changing stage to: {0}", state);
        if (stage != null) {
            stage.stop();
            stage = null;
        }
        switch (state) {
            case HOME:
                stage = new HomeStage(keyListener);
                break;
            case CALIBRATION:
                stage = new CalibrationStage(keyListener, playerTracker, cameraConnection);
                break;
            case PLAY:
                stage = new PlayStage(keyListener, playerTracker);
                break;
            case QUIT:
                engine.close();
                break;
        }
    }

    @Override
    public void renderContent(Graphics2D g, GameRenderer renderer) {
        try {
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, renderer.getScreenImage().getWidth(), renderer.getScreenImage().getHeight());

            AffineTransform t = g.getTransform();
            g.translate(renderXOffset, renderYOffset);

            if (stage != null) {
                Renderable view = stage.getView();

                view.renderContent(g, renderer);

                if (view instanceof CalibrationView) {
                    ((CalibrationView) view).showImage(cameraImage, g, renderer);
                }
            }

            if (cameraStatusRenderer != null) {
                cameraStatusRenderer.renderContent(g, renderer);
            }

            int screenWidth = Config.getPixelWidth();
            int screenHeight = Config.getPixelHeight();
            int letterbox = renderer.getScreenImage().getWidth() - screenWidth;
            g.setColor(Color.DARK_GRAY.darker());

            g.fillRect(screenWidth, 0, letterbox / 2, screenHeight);
            g.fillRect(-letterbox / 2, 0, letterbox / 2, screenHeight);

            g.setColor(Color.DARK_GRAY);
            g.drawRect(-1, -1, screenWidth + 1, screenHeight + 1);

            g.setTransform(t);
        } catch (Exception e) {
            Logger.getGlobal().log(Level.WARNING, e.getMessage());
        }
    }

    private void toggleFullScreen() {
        fullScreen = !fullScreen;
        if (fullScreen) {
            resizeToFullScreen();
        } else {
            resizeToNormal();
        }
    }

}
