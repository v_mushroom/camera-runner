/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.camera;

import com.mush.cameratrack.CameraImageGrabber;
import com.mush.game.utils.core.GameEventQueue;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class CameraConnection implements Runnable {

    public static String QUEUE = "CAMERA";

    public static class ImageGrabbedEvent {

        public final BufferedImage image;

        public ImageGrabbedEvent(BufferedImage image) {
            this.image = image;
        }
    }

    private CameraImageGrabber grabber;
    private boolean connected = false;
    private boolean connecting = false;
    private boolean loop = true;
    private boolean active = false;

    public CameraConnection() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loop = false;
                disconnect();
            }
        });
    }

    public void setup() {
        new Thread(this).start();
    }

    @Override
    public void run() {
        while (loop) {
            if (active) {
                loopConnect();
            } else {
                sleep(1000);
            }
        }
    }

    private void loopConnect() {
        if (loop && !connected) {
            connect();
        }
        if (loop && (connected || connecting) && grabber != null) {
            BufferedImage image = grabber.grab();
            if (image == null) {
                disconnect();
            } else {
                GameEventQueue.category(QUEUE).sendEvent(new ImageGrabbedEvent(image));
            }
        }
        if (loop && !connected) {
            Logger.getGlobal().log(Level.INFO, "Waiting before retrying to connect");
            sleep(1000);
        }
    }

    private void sleep(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Logger.getLogger(CameraConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void connect() {
        try {
            Logger.getGlobal().log(Level.INFO, "Connecting to camera");
            connecting = true;
            grabber = new CameraImageGrabber();
            connected = true;
            connecting = false;
            Logger.getGlobal().log(Level.INFO, "Connected");
        } catch (Exception ex) {
            Logger.getLogger(CameraConnection.class.getName()).log(Level.SEVERE, null, ex);
            grabber = null;
            connected = false;
            connecting = false;
        }
    }

    private void disconnect() {
        Logger.getGlobal().log(Level.INFO, "Disconnecting camera");
        if (grabber != null) {
            grabber.stop();
        }
        connected = false;
        connecting = false;
    }

    public boolean isConnected() {
        return connected;
    }

    public boolean isConnecting() {
        return connecting;
    }

    public void setActive(boolean active) {
        Logger.getGlobal().log(Level.INFO, "Set camera active to {0}", active);
        boolean wasActive = this.active;
        this.active = active;
        if (wasActive && connected && !active) {
            disconnect();
        }
    }
    
    public boolean getActive() {
        return active;
    }

}
