/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.camera;

import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.render.Renderable;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *
 * @author mush
 */
public class CameraStatusOverlay implements Renderable {

    private final CameraConnection cameraConnection;
    private final PlayerTracker playerTracker;
    private boolean showTracking = true;

    public CameraStatusOverlay(CameraConnection cameraConnection, PlayerTracker playerTracker) {
        this.cameraConnection = cameraConnection;
        this.playerTracker = playerTracker;
    }

    public void setShowTracking(boolean showTracking) {
        this.showTracking = showTracking;
    }

    @Override
    public void renderContent(Graphics2D g, GameRenderer renderer) {
        AffineTransform t = g.getTransform();
        g.translate(0, 0);
        renderStatus(g);
        if (showTracking) {
            g.translate(10, 0);
            renderTracking(g);
        }
        g.setTransform(t);
    }

    private void renderStatus(Graphics2D g) {
        if (cameraConnection.isConnected()) {
            g.setColor(Color.GREEN);
        } else if (cameraConnection.isConnecting()) {
            g.setColor(Color.YELLOW);
        } else {
            g.setColor(Color.RED);
        }

        g.fillRect(0, 0, 5, 5);
    }

    private void renderTracking(Graphics2D g) {
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, 10, 5);

        g.setColor(Color.GRAY);

        g.fillRect(0, 0, 10, 1);
        g.fillRect(0, 2, 10, 1);
        g.fillRect(0, 4, 10, 1);

        g.setColor(playerTracker.getAnalyzerConfidence()> 0.5 ? Color.GREEN : Color.RED);
        g.fillRect(0, 0, (int) (10 * playerTracker.getAnalyzerConfidence()), 1);

        g.setColor(playerTracker.getTrackerConfidence()> 0.5 ? Color.GREEN : Color.RED);
        g.fillRect(0, 2, (int) (10 * playerTracker.getTrackerConfidence()), 1);

        double confidence = playerTracker.getConfidence();
        if (confidence > 0.5) {
            g.setColor(Color.GREEN);
            g.fillRect(0, 4, (int) (10 * confidence), 1);
        } else {
            g.setColor(Color.RED);
            int width = (int) (10 * (1 - confidence));
            g.fillRect(10 - width, 4, width, 1);
        }

        g.translate(11, 0);
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, 100, 4);

        g.setColor(Color.GRAY);
        g.fillRect(0, 0, 100, 1);
        g.fillRect(0, 2, 100, 1);
        g.fillRect(50, 1, 1, 3);
        
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(20, 0, 1, 1);
        g.fillRect(40, 0, 1, 1);
        g.fillRect(60, 0, 1, 1);
        g.fillRect(80, 0, 1, 1);

        g.setColor(Color.YELLOW);
        g.fillRect(0, 0, (int) (20 * playerTracker.getJogFrequency()), 1);

        g.setColor(Color.YELLOW);
        double position = playerTracker.getPlayerPosition() / 2 + 0.5;
        g.fillRect((int) (99 * position), 1, 1, 3);
    }

}
