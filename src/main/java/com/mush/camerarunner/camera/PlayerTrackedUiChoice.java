/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.camera;

import com.mush.camerarunner.Config;
import com.mush.game.utils.core.GameEventQueue;
import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class PlayerTrackedUiChoice {

    class Choice {

        public String text;
        public Object action;
        public double position;
        public double width;
        public double complete = 0;
        public boolean completed = false;

        public Choice(String text, Object action, double position, double width) {
            this.text = text;
            this.action = action;
            this.position = position;
            this.width = width;
        }

        public void update(double elapsedSeconds) {
            if (completed) {
                return;
            }

            double player = -playerTracker.getPlayerPosition();
            if (position < player + width / 2 && position > player - width / 2) {
                complete += 0.1 * playerTracker.getJogFrequency() * elapsedSeconds;
            } else {
                complete = 0;
            }

            if (complete > 1) {
                completed = true;
                complete = 1;
                if (action != null) {
                    GameEventQueue
                            .category(GameEventQueue.Categories.NAVIGATION)
                            .sendEvent(action);
                }
            }
        }
    }

    private PlayerTracker playerTracker;
    private List<Choice> choices;

    public PlayerTrackedUiChoice(PlayerTracker playerTracker) {
        this.playerTracker = playerTracker;
        choices = new ArrayList<>();
    }

    public void addChoice(String text, Object action, double position, double width) {
        Choice choice = new Choice(text, action, position, width);
        choices.add(choice);
    }

    public void update(double elapsedSeconds) {
        choices.forEach((choice) -> {
            choice.update(elapsedSeconds);
        });
    }
    
    public void reset() {
        choices.forEach((choice) -> {
            choice.completed = false;
            choice.complete = 0;
        });
    }

    public void draw(Graphics2D g) {
        int centerX = Config.getPixelWidth() / 2;
        int y = (int) (Config.getPixelHeight() * 0.9);
        int halfWidth = (int) (0.75 * Config.getPixelWidth() / 2);

        g.setColor(Color.GRAY);
        g.fillRect(centerX - halfWidth, y, 2 * halfWidth, 5);
        g.setColor(Color.YELLOW);
        g.fillRect(centerX - halfWidth, y + 2, 2 * halfWidth, 1);
        g.fillRect((int) (centerX - playerTracker.getPlayerPosition() * halfWidth), y, 1, 5);

        choices.forEach((choice) -> {
            draw(g, choice, centerX, halfWidth, y);
        });
    }

    private void draw(Graphics2D g, Choice choice, int centerX, int halfWidth, int y) {
        int x = (int) (centerX + choice.position * halfWidth);
        int span = (int) (halfWidth * choice.width);
        g.setColor(Color.YELLOW);
        g.fillRect(x - span / 2, y + 3, span, 1);

        int textWidth = g.getFontMetrics().stringWidth(choice.text);
        int textHeight = 10;
        g.setColor(Color.DARK_GRAY);
        g.fillRect(x - textWidth / 2 - 1, y - textHeight - 1, textWidth + 2, 10 + 2);
        g.setColor(Color.LIGHT_GRAY);
        g.drawRect(x - textWidth / 2 - 1, y - textHeight - 1, textWidth + 1, 10 + 1);
        int completeWidth = (int) (textWidth * choice.complete);
        g.fillRect(x - textWidth / 2, y - textHeight, completeWidth, 10);

        g.setColor(Color.BLACK);
        g.drawString(choice.text, x - textWidth / 2, y);
    }
}
