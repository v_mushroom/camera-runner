/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.camera;

import com.mush.cameratrack.ColorFinder;
import com.mush.cameratrack.PatchFinder;
import com.mush.cameratrack.PatchTracker;
import com.mush.cameratrack.Sequence;
import com.mush.cameratrack.SequenceAnalyzer;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author mush
 */
public class PlayerTracker {

    public static final int CALIBRATION_WIDTH = 5;
    public static final int CALIBRATION_HEIGHT = 5;
    private static final int CALIBRATION_MIDDLE = CALIBRATION_WIDTH * CALIBRATION_HEIGHT / 2;

    public final PatchFinder finder;
    private final PatchTracker tracker;
    private final SequenceAnalyzer analyzer;
    private float[] calibrationHues;
    private Color[] calibrationColors;
    private float[] calibrationCenterHsv;
    private double confidence;
    private double leftRangeBound = 0;
    private double rightRangeBound = 1;
    private double rangeSpan = 1;
    private boolean calibratingColor = false;
    private double sampleIntervalSeconds = 1;
    private long lastSampleMillis = 0;
    private Sequence frequencySequence;
    private double averageFrequency = 0;
    private Sequence playerPositionSequence;
    private double averagePlayerPosition;
    public Rectangle2D foundPatch;
    public BufferedImage lastImage;

    public PlayerTracker() {
        finder = new PatchFinder();
        tracker = new PatchTracker();
        analyzer = new SequenceAnalyzer(20);
        frequencySequence = new Sequence(10);
        playerPositionSequence = new Sequence(5);

        calibrationHues = new float[CALIBRATION_WIDTH * CALIBRATION_HEIGHT];
        calibrationColors = new Color[calibrationHues.length];

        setLeftRangeBound(0.25);
        setRightRangeBound(0.75);
    }
    
    public void reset() {
        averageFrequency = 0;
        averagePlayerPosition = 0;
        frequencySequence.clear();
        playerPositionSequence.clear();
        analyzer.clear();
    }

    private void updateSampleRate() {
        long millis = System.currentTimeMillis();
        if (lastSampleMillis != 0) {
            long intervalMillis = millis - lastSampleMillis;
            double intervalSeconds = intervalMillis / 1000.0;
            sampleIntervalSeconds = sampleIntervalSeconds * 0.75 + intervalSeconds * 0.25;
            if (sampleIntervalSeconds <= 0) {
                sampleIntervalSeconds = 1;
            }
        }
        lastSampleMillis = millis;
    }

    public void process(BufferedImage image) {
        updateSampleRate();

        lastImage = image;
        foundPatch = finder.findPatch(image);
        tracker.track(foundPatch);
        if (foundPatch != null) {
            analyzer.add(foundPatch.getMinY());
        }

        frequencySequence.add(currentJogFrequency(analyzer.frequency * getConfidence()));
        averageFrequency = frequencySequence.average();
        playerPositionSequence.add(centeredPlayerPosition());
        averagePlayerPosition = playerPositionSequence.average();

        for (int i = 0; i < calibrationColors.length; i++) {
            int ix = i % CALIBRATION_WIDTH;
            int iy = i / CALIBRATION_WIDTH;
            double u = 0.5 + (ix - CALIBRATION_WIDTH / 2) * 0.1;
            double v = 0.5 + (iy - CALIBRATION_HEIGHT / 2) * 0.1;
            float[] hsv = PatchFinder.getPixelHsvAtUv(u, v, image);
            calibrationHues[i] = hsv[0];
            calibrationColors[i] = ColorFinder.getHueColor(calibrationHues[i]);
            if (i == CALIBRATION_MIDDLE) {
                calibrationCenterHsv = hsv;
            }
        }

        if (calibratingColor) {
            calibrateColor();
        }

        double newConfidence = analyzer.confidence * tracker.getConfidence();
        if (newConfidence < confidence) {
            confidence = newConfidence;
        } else {
            confidence = confidence * 0.75 + newConfidence * 0.25;
        }
    }

    private void calibrateColor() {
        float centerHue = calibrationHues[CALIBRATION_WIDTH * (CALIBRATION_HEIGHT / 2) + CALIBRATION_WIDTH / 2];
        for (int u = 1; u <= CALIBRATION_WIDTH - 2; u++) {
            for (int v = 1; v <= CALIBRATION_HEIGHT - 2; v++) {
                int i = v * CALIBRATION_WIDTH + u;
                float offset = ColorFinder.getHueOffset(calibrationHues[i], centerHue);
                if (offset > 1 - finder.colorFinder.hueTreshold) {
                    return;
                }
            }
        }
        finder.colorFinder.targetHsv = calibrationCenterHsv;
        calibratingColor = false;
    }

    public double getJogFrequency() {
//        return currentJogFrequency(analyzer.frequency);
//        return currentJogFrequency(averageFrequency);
        return averageFrequency;
    }
    
    private double currentJogFrequency(double frequency) {
        double minAmplitude = 0.02;
//        double frequency = analyzer.frequency;
        if (frequency < 0.05) {
            frequency = 0;
        }
        if (analyzer.amplitude < minAmplitude) {
            double f = analyzer.amplitude / minAmplitude;
            frequency *= f;
        }
        frequency /= sampleIntervalSeconds;
        if (frequency > 5) {
            // 5 herz is practially impossible to detect reliably
            frequency = 5;
        }
        return frequency;
    }

    public final void setLeftRangeBound(double left) {
        double range = rightRangeBound - left;
        if (range > 0) {
            leftRangeBound = left;
            rangeSpan = range;
        }
    }

    public final void setRightRangeBound(double right) {
        double range = right - leftRangeBound;
        if (range > 0) {
            rightRangeBound = right;
            rangeSpan = range;
        }
    }

    public double getLeftRangeBound() {
        return leftRangeBound;
    }

    public double getRightRangeBound() {
        return rightRangeBound;
    }

    public double getPlayerPosition() {
        return averagePlayerPosition;
    }
    
    private double centeredPlayerPosition() {
        double rawPosition = tracker.getCurrent().getCenterX();
        double boundPosition = (rawPosition - leftRangeBound) / rangeSpan;
        if (boundPosition < 0) {
            boundPosition = 0;
        } else if (boundPosition > 1) {
            boundPosition = 1;
        }
        return (boundPosition - 0.5) * 2;
    }

    public double getConfidence() {
        return confidence;
    }

    public double getAnalyzerConfidence() {
        return analyzer.confidence;
    }

    public double getTrackerConfidence() {
        return tracker.getConfidence();
    }

    public Color[] getCenterColors() {
        return calibrationColors;
    }

    public float[] getCalibrationCenterHsv() {
        return calibrationCenterHsv;
    }

    public Color getTrackedColor() {
        return ColorFinder.getHueColor(finder.colorFinder.targetHsv);
    }

    public boolean isCalibratingColor() {
        return calibratingColor;
    }

    public void setCalibratingColor(boolean calibratingColor) {
        this.calibratingColor = calibratingColor;
    }

}
