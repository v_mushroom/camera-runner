/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner;

/**
 *
 * @author mush
 */
public class Config {

    public final static int VIEW_WIDTH = 640;
    public final static int VIEW_HEIGHT = 480;
    public final static int PIXEL_SIZE = 2;
    
    public final static int TILE_WIDTH = 16;
    public final static int TILE_HEIGHT = 16;

    public static int getPixelWidth() {
        return VIEW_WIDTH / PIXEL_SIZE;
    }

    public static int getPixelHeight() {
        return VIEW_HEIGHT / PIXEL_SIZE;
    }
}
