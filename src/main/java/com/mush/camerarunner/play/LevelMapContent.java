/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import com.mush.camerarunner.Config;
import com.mush.game.utils.map.FixedSizeMapContent;
import com.mush.game.utils.map.MapBoundary;
import com.mush.game.utils.map.MapContent;
import com.mush.game.utils.map.MapContentComparator;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mush
 */
public class LevelMapContent implements MapContent<MapMaterial> {

    private final FixedSizeMapContent<MapMaterial> map;

    private final MapMaterials mapMaterials;

    public final MapMaterial GRASS;
    public final MapMaterial DIRT;
    public final MapMaterial ROAD;
    public final MapMaterial WATER;
    public final MapMaterial POTATO;
//    public final MapMaterial FINISH;
    public final MapMaterial FLOWER;
    public final MapMaterial BRIDGE;

    public final MapContentComparator<MapMaterial> comparator;

    private final int screenTileWidth = Config.getPixelWidth() / Config.TILE_WIDTH;
    private final int screenTileHeight = Config.getPixelHeight() / Config.TILE_HEIGHT;
    public final int finishLineV;
    
    public List<MapPoi> randomPoiList;

    public LevelMapContent(MapMaterials mapMaterials, MapContentComparator<MapMaterial> comparator) {
        this.mapMaterials = mapMaterials;

        GRASS = mapMaterials.getMaterialByName("GRASS");
        DIRT = mapMaterials.getMaterialByName("DIRT");
        ROAD = mapMaterials.getMaterialByName("ROAD");
        WATER = mapMaterials.getMaterialByName("WATER");
        POTATO = mapMaterials.getMaterialByName("POTATO");
//        FINISH = mapMaterials.getMaterialByName("FINISH");
        FLOWER = mapMaterials.getMaterialByName("FLOWER");
        BRIDGE = mapMaterials.getMaterialByName("BRIDGE");

        this.map = new FixedSizeMapContent<>(screenTileWidth, 10 * screenTileHeight);

//        comparator = new MapMaterialComparator(mapMaterials);
        this.comparator = comparator;
        this.comparator.setMapContent(this);

        finishLineV = screenTileHeight;
        
        randomPoiList = new ArrayList<>();

//        generateMap();
    }

    public int getWidth() {
        return map.getWidth();
    }

    public int getHeight() {
        return map.getHeight();
    }

    public void generateMap() {
        for (int u = 0; u < map.getWidth(); u++) {
            for (int v = 0; v < map.getHeight(); v++) {
                map.setItemAt(u, v, GRASS);
            }
        }
        
        randomPoiList.clear();

        for (int i = 0; i < 3 + Math.random() * 3; i++) {
            generateWater();
        }

        generateRoad();

        int countSideRoads = 0;
        for (int i = 0; i < 10 && countSideRoads < 5; i++) {
            if (generateSideRoad()) {
                countSideRoads++;
            }
        }
//        generateFinishLine();

        generateFlowers();

        cleanup(WATER, DIRT);
        cleanup(BRIDGE, ROAD);

//        generatePatterns();
    }

    private void generateFlowers() {
        for (int i = 0; i < 200; i++) {
            int u = 1 + (int) (Math.random() * (map.getWidth() - 3));
            int v = 1 + (int) (Math.random() * (map.getHeight() - 3));

            if (GRASS == map.getItemAt(u, v)) {
                MapBoundary.TileType type = MapBoundary.evaluate(u, v, comparator);
                if (MapBoundary.TileType.ALL == type) {
                    map.setItemAt(u, v, FLOWER);
                }
            }
        }
    }

    private void generatePatterns() {
        int u = 1;
        int v = 1;
        for (int i = 0; i < 256; i++) {
            pattern(u, v, WATER, i);
            u += 4;
            if (u + 2 > map.getWidth()) {
                u = 1;
                v += 4;
            }
        }
    }

    private void pattern(int u, int v, MapMaterial material, int pattern) {
        map.setItemAt(u, v, material);
        int bit = 1;
        map.setItemAt(u - 1, v - 1, (pattern & bit) != 0 ? material : GRASS);
        bit <<= 1;
        map.setItemAt(u + 0, v - 1, (pattern & bit) != 0 ? material : GRASS);
        bit <<= 1;
        map.setItemAt(u + 1, v - 1, (pattern & bit) != 0 ? material : GRASS);
        bit <<= 1;
        map.setItemAt(u + 1, v + 0, (pattern & bit) != 0 ? material : GRASS);
        bit <<= 1;
        map.setItemAt(u + 1, v + 1, (pattern & bit) != 0 ? material : GRASS);
        bit <<= 1;
        map.setItemAt(u + 0, v + 1, (pattern & bit) != 0 ? material : GRASS);
        bit <<= 1;
        map.setItemAt(u - 1, v + 1, (pattern & bit) != 0 ? material : GRASS);
        bit <<= 1;
        map.setItemAt(u - 1, v + 0, (pattern & bit) != 0 ? material : GRASS);
    }

    private void cleanup(MapMaterial material, MapMaterial replacemet) {
        for (int u = 0; u < map.getWidth(); u++) {
            for (int v = 0; v < map.getHeight(); v++) {
                MapMaterial materialAt = map.getItemAt(u, v);
                if (material == materialAt) {
                    MapBoundary.TileType type = MapBoundary.evaluate(u, v, comparator);
                    if (MapBoundary.TileType.SINGLE == type) {
                        map.setItemAt(u, v, replacemet);
                    }
                }
            }
        }
    }

    public MapBoundary.TileType getBoundaryTypeAt(int u, int v) {
        return MapBoundary.evaluate(u, v, comparator);
    }

    private void generateWater() {
        int u = 1 + (int) (Math.random() * (map.getWidth() - 3));
        int v = 1 + (int) (Math.random() * (map.getHeight() - 3));

        setWater(u, v);

        for (int i = 0; i < 200; i++) {
            if (Math.random() < 0.5) {
                if (u < 1) {
                    u += 1;
                } else if (u > map.getWidth() - 3) {
                    u -= 1;
                } else {
                    u += Math.random() < 0.5 ? -1 : 1;
                }
            } else {
                if (v < 2) {
                    v += 1;
                } else if (v > map.getHeight() - 4) {
                    v -= 1;
                } else {
                    v += Math.random() < 0.5 ? -1 : 1;
                }
            }
            setWater(u, v);
        }
    }

    private void setWater(int u, int v) {
        map.setItemAt(u, v, WATER);
        map.setItemAt(u + 1, v, WATER);
        map.setItemAt(u, v + 1, WATER);
        map.setItemAt(u + 1, v + 1, WATER);
    }

    /*
    private void generateFinishLine() {
        int v = finishLineV;
        for (int u = 0; u < map.getWidth(); u++) {
            map.setItemAt(u, v, FINISH);
        }
    }
     */
    private void generateRoad() {
        int left = map.getWidth() / 2 - 1;
        int right = map.getWidth() / 2 + 1;
        int leftChanged = 0;
        int rightChanged = 0;
        int maxLeft = map.getWidth() / 2 - 7;
        int maxRight = map.getWidth() / 2 + 7;
        int wait = 5;

        for (int i = 0; i < map.getHeight(); i++) {
            int v = map.getHeight() - i - 1;
            boolean water = false;
            for (int u = left; u < right; u++) {
                if (map.getItemAt(u, v) == WATER) {
                    water = true;
                    map.setItemAt(u, v, BRIDGE);
                } else {
                    map.setItemAt(u, v, ROAD);
                    if (v > finishLineV && Math.random() < 0.05) {
                        randomPoiList.add(new MapPoi(MapPoi.Type.MAIN_ROAD, u, v));
                    }
                }
            }
            if (wait <= 0 && !water) {
                if (Math.random() < 0.75 && leftChanged > 1) {
                    left += Math.random() < 0.5 ? -1 : 1;
                    left = Math.min(left, right - 2);
                    left = Math.max(left, right - 5);
                    left = Math.max(left, maxLeft);
                    leftChanged = 0;
                }
                if (Math.random() < 0.75 && rightChanged > 1) {
                    right += Math.random() < 0.5 ? -1 : 1;
                    right = Math.max(right, left + 2);
                    right = Math.min(right, left + 5);
                    right = Math.min(right, maxRight);
                    rightChanged = 0;
                }
                leftChanged++;
                rightChanged++;
            }
            wait--;
        }
    }

    private boolean generateSideRoad() {
        int maxLeft = map.getWidth() / 2 - 6;
        int maxRight = map.getWidth() / 2 + 6;
        int v = 5 + (int) (Math.random() * (map.getHeight() - 10));
        int minU = 0;
        int maxU = map.getWidth();
        for (int u = 0; u < map.getWidth(); u++) {
            if (map.getItemAt(u, v) == ROAD) {
                if (minU == 0) {
                    minU = u;
                }
                maxU = u;
            }
        }
        int u;
        int du;
        if (minU > map.getWidth() * 0.5) {
            u = minU;
            du = -1;
        } else if (maxU < map.getWidth() * 0.5) {
            u = maxU;
            du = 1;
        } else {
            return false;
        }
//        System.out.println("Start" + u + " v " + v);

        boolean found = false;
        boolean delivered = false;
        int length = 0;
        int wait = 2;
        while (v > 0 && !found) {
            MapMaterial here = map.getItemAt(u, v);
            boolean water = here == WATER;
            if (wait <= 0) {
                if (isRoad(here)) {
//                    System.out.println("Found u " + u + " v " + v);
                    found = true;
                }
                if (Math.random() < 0.5 && !water) {
                    du += Math.random() < 0.5 ? -1 : 1;
                    du = du < 0 ? -1 : du;
                    du = du > 0 ? +1 : du;
                }
                if (u >= maxRight) {
                    du = -1;
                } else if (u <= maxLeft) {
                    du = 1;
                }
            }
//            System.out.println("Set u " + u + " v " + v);
            if (water) {
                map.setItemAt(u, v, BRIDGE);
//                System.out.println("Set u+1 " + (u + 1) + " v " + v);
                map.setItemAt(u + 1, v, BRIDGE);
            } else {
                map.setItemAt(u, v, ROAD);
                if (du != 0) {
                    u += du;
                    here = map.getItemAt(u, v);
                    if (isRoad(here) && wait <= 0) {
                        found = true;
//                    System.out.println("Found u+du " + u + " v " + v);
                    }
//                System.out.println("Set u+du " + u + " v " + v);
                    map.setItemAt(u, v, ROAD);
                }
                if (length > 5 && !delivered && v > finishLineV && Math.random() < 0.5) {
                    delivered = true;
                    randomPoiList.add(new MapPoi(MapPoi.Type.SIDE_ROAD, u, v));
                }
            }

            v--;
            wait--;
            length++;
        }
        return true;
    }

    private boolean isRoad(MapMaterial mat) {
        return mat == ROAD || mat == BRIDGE/* || mat == POTATO*/;
    }

    @Override
    public MapMaterial getItemAt(int u, int v) {
//        MapMaterial tile = map.getItemAt(u, v + map.getHeight() - Config.getPixelHeight() / Config.TILE_HEIGHT);
        MapMaterial tile = map.getItemAt(u, v);

        if (tile == null) {
            if (u < 0) {
                tile = map.getItemAt(0, v);
            } else if (u >= map.getWidth()) {
                tile = map.getItemAt(map.getWidth() - 1, v);
            }
            return tile != null ? tile : GRASS;
        } else {
            return tile;
        }
    }

    @Override
    public void setItemAt(int u, int v, MapMaterial item) {
        map.setItemAt(u, v, item);
    }

}
