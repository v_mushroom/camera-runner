/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import com.mush.camerarunner.Config;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import com.mush.game.utils.map.material.MapMaterialsLoader;
import com.mush.game.utils.tiles.MapContentSurfaceDataSource;
import com.mush.game.utils.tiles.MaterialDataSourceLoader;
import com.mush.game.utils.tiles.TiledSurface;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class LevelMap {

    public TiledSurface surface;
    public MapContentSurfaceDataSource<MapMaterial> dataSource;
    public LevelMapContent mapContent;
    public MapMaterials mapMaterials;

    public LevelMap() {
        MapMaterialComparator comparator = null;
        try {
            mapMaterials = MapMaterialsLoader.loadFromFile("res/materials.json");
            comparator = new MapMaterialComparator(mapMaterials);
            dataSource = MaterialDataSourceLoader.loadFromFile("res/materials.json", mapMaterials, comparator);
        } catch (Exception e) {
            // worry about it later
            Logger.getGlobal().log(Level.SEVERE, "", e);
        }

        surface = new TiledSurface(Config.getPixelWidth(), Config.getPixelHeight(), Config.TILE_WIDTH, Config.TILE_HEIGHT);
        
        mapContent = new LevelMapContent(mapMaterials, dataSource.getMapContentComparator());
        
        dataSource.setMapContent(mapContent);
        surface.setDataSource(dataSource);
    }
}
