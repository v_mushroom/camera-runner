/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import com.mush.game.utils.map.move.MovingObject;
import com.mush.game.utils.sprites.Sprite;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mush
 */
public class Character extends MovingObject {

    public CharacterState state;
    public final CharacterType type;

    protected Map<CharacterState, String> spriteStateNameMap;
    protected Map<CharacterState, CharacterState> stateTransitionMap;

    public Character(Sprite sprite, CharacterType type) {
        super();
        this.type = type;
        this.sprite = sprite;
        spriteStateNameMap = new HashMap<>();
        stateTransitionMap = new HashMap<>();
        
        if (sprite != null) {
            setSpriteStateName(CharacterState.IDLE, CharacterState.SPRITE_STATE_IDLE);
        }
        setState(CharacterState.IDLE);
    }

    public void setSpriteStateName(CharacterState state, String spriteStateName) {
        spriteStateNameMap.put(state, spriteStateName);
    }
    
    public void addStateTransition(CharacterState state, CharacterState nextState) {
        stateTransitionMap.put(state, nextState);
    }
    
    public void setState(CharacterState state) {
        if (state != this.state) {
            this.state = state;
            String spriteState = spriteStateNameMap.get(this.state);
            if (this.sprite != null && spriteState != null) {
                this.sprite.setState(spriteState);
            }
        }
    }
    
    public boolean hasStateTransition(CharacterState state) {
        return stateTransitionMap.containsKey(state);
    }

    /**
     * @param elapsedSeconds
     * @return false if mob is to be removed
     */
    public boolean update(double elapsedSeconds) {
        if (this.sprite != null) {
            this.sprite.update(elapsedSeconds);
            if (this.sprite.isAnimationFinished()) {
                CharacterState nextState = stateTransitionMap.get(this.state);
                if (nextState == null) {
                    return false;
                }
                setState(nextState);
            }
        }
        return true;
    }
}
