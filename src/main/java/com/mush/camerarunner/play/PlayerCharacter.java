/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import com.mush.camerarunner.Config;
import com.mush.game.utils.sprites.Sprite;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author mush
 */
public class PlayerCharacter extends MovingCharacter {

    public static final String SPRITE_STATE_RUNNING = "running";
    public static final String SPRITE_STATE_STUMBLING = "stumbling";

    public double playerWalkStateDuration = 0;

    public PlayerCharacter(Sprite sprite) {
        super(sprite, CharacterType.PLAYER);
        setSpriteStateName(CharacterState.RUNNING, SPRITE_STATE_RUNNING);
        setSpriteStateName(CharacterState.STUNNED, SPRITE_STATE_STUMBLING);
        controls = new Point2D.Double(0, 0);
//        bounds.set(Config.TILE_WIDTH / 2, Config.TILE_HEIGHT / 4, Config.TILE_WIDTH / 2, 0);
        BufferedImage image = sprite.getImage();
        bounds.set(image.getWidth() / 2, Config.TILE_HEIGHT / 4, image.getWidth() / 2, 0);
    }

    @Override
    public boolean isControlledState() {
        return state == CharacterState.IDLE || state == CharacterState.RUNNING;
    }

    @Override
    protected void applyControlledState(double elapsedSeconds) {
        super.applyControlledState(elapsedSeconds);

        boolean playerWalking = controls.x != 0 || controls.y != 0;

        if (playerWalking) {
            setState(CharacterState.RUNNING);
        } else if (playerWalkStateDuration > 0.3) {
            setState(CharacterState.IDLE);
            playerWalkStateDuration = 0;
        }

        if (CharacterState.RUNNING == state) {
            playerWalkStateDuration += elapsedSeconds;
        }
    }

    @Override
    protected void applyUncontrolledState(double elapsedSeconds) {
        velocity.x -= velocity.x * 2 * elapsedSeconds;
        velocity.y -= velocity.y * 2 * elapsedSeconds;
        if (Math.abs(velocity.x) + Math.abs(velocity.y) < 1) {
            setState(CharacterState.IDLE);
        }
    }

}
