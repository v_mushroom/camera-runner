/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

/**
 *
 * @author mush
 */
public enum CharacterState {
    IDLE,
    RUNNING,
    ACTIVATING, // transition
    ACTIVATING2, // transition, maybe do this diferently, one state, string of animations
    ACTIVATED, // like picked up, walked over, etc
    DEACTIVATING, // transition
    STUNNED;

    public static final String SPRITE_STATE_IDLE = "default";
    public static final String SPRITE_STATE_ACTIVATING = "activating";
    public static final String SPRITE_STATE_ACTIVATING2 = "activating:2";
    public static final String SPRITE_STATE_ACTIVATED = "activated";
    public static final String SPRITE_STATE_DEACTIVATING = "deactivating";

}
