/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import com.mush.game.utils.sprites.Sprite;
import java.awt.geom.Point2D;

/**
 *
 * @author mush
 */
public class MovingCharacter extends Character {

    public Point2D.Double controls;
    public double movementSpeedFactor = 1.0;

    public MovingCharacter(Sprite sprite, CharacterType type) {
        super(sprite, type);
        controls = new Point2D.Double(0, 0);
    }

    public void applyControls(double elapsedSeconds) {
        if (isControlledState()) {
            applyControlledState(elapsedSeconds);
        } else {
            applyUncontrolledState(elapsedSeconds);
        }
    }

    public boolean isControlledState() {
        return true;
    }

    protected void applyControlledState(double elapsedSeconds) {
        velocity.x = controls.x * movementSpeedFactor;
        velocity.y = controls.y * movementSpeedFactor;
    }

    protected void applyUncontrolledState(double elapsedSeconds) {
        
    }

}
