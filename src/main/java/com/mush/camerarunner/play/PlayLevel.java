/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import com.mush.camerarunner.Config;
import static com.mush.camerarunner.stages.PlayStage.PLAY_QUEUE;
import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.map.MapBoundary;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.move.MovingObject;
import com.mush.game.utils.map.move.MovingObjects;
import com.mush.game.utils.sprites.Sprite;
import com.mush.game.utils.sprites.SpriteFactory;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class PlayLevel {

    public MovingObjects<MapMaterial> mobs;
    public PlayerCharacter playerMob;
    public Character finishMob;

    public LevelMap map;

    public boolean finished = false;
    public int pickedUpCount;

    private List<MovingObject> finishLineSegments;
    private List<Character> temporaryCharacters;
    private SpriteFactory spriteFactory;
    private CharacterFactory characterFactory;

    public PlayLevel(SpriteFactory spriteFactory) {
        this.spriteFactory = spriteFactory;
        characterFactory = new CharacterFactory(spriteFactory);

        map = new LevelMap();
        map.surface.setSmoothTranslate(false);

        mobs = new MovingObjects<>();

        playerMob = new PlayerCharacter(spriteFactory.createSprite("PLAYER"));
        playerMob.bounds.set(Config.TILE_WIDTH / 2, Config.TILE_HEIGHT / 4, Config.TILE_WIDTH / 2, 0);
        mobs.add(playerMob);

        createFinishLine();

        temporaryCharacters = new ArrayList<>();

        GameEventQueue.category(PLAY_QUEUE).addEventListener(this);
    }

    public void cleanup() {
        GameEventQueue.category(PLAY_QUEUE).removeEventListener(this);
    }

    private void createFinishLine() {
        finishMob = new Character(null, CharacterType.FINISH);
        finishMob.bounds.set(0, 0, Config.getPixelWidth(), Config.TILE_HEIGHT / 4);
        finishMob.isObstacleForMobs = false;
        mobs.add(finishMob);

        finishLineSegments = new ArrayList<>();
        Sprite finishSprite = spriteFactory.createSprite("FINISH_LINE");
        finishSprite.setState(CharacterState.SPRITE_STATE_IDLE);

        for (int i = 0; i < map.mapContent.getWidth() * Config.TILE_WIDTH / finishSprite.getImage().getWidth(); i++) {
            MovingObject segment = new MovingObject();
            segment.bounds.set(0, 0, finishSprite.getImage().getWidth(), finishSprite.getImage().getHeight());
            segment.sprite = finishSprite;
            segment.collidesWithMap = false;
            segment.collidesWithMobs = false;
            finishLineSegments.add(segment);
            mobs.add(segment);
        }
    }

    public void reset() {
        temporaryCharacters.forEach((mob) -> {
            if (mob.id != null) {
                mobs.remove(mob.id);
            }
        });
        temporaryCharacters.clear();

        map.mapContent.generateMap();

        map.mapContent.randomPoiList.forEach((poi) -> {
            Character poiCharacter = null;
            switch (poi.type) {
                case SIDE_ROAD:
                    poiCharacter = characterFactory.potato();
                    break;
                case MAIN_ROAD:
                    poiCharacter = characterFactory.mushroom();
                    break;
            }
            if (poiCharacter != null) {
                poiCharacter.position.setLocation(poi.point.x * Config.TILE_WIDTH + Config.TILE_WIDTH / 2, poi.point.y * Config.TILE_HEIGHT + Config.TILE_HEIGHT / 2);
                poiCharacter.sprite.update(Math.random() * 10);
                temporaryCharacters.add(poiCharacter);
                mobs.add(poiCharacter);
            }
        });

        finished = false;
        pickedUpCount = 0;

        playerMob.setState(CharacterState.IDLE);
        playerMob.position.setLocation(
                map.mapContent.getWidth() * Config.TILE_WIDTH / 2,
                map.mapContent.getHeight() * Config.TILE_HEIGHT - Config.getPixelHeight() * 1.0 / 3);
        playerMob.velocity.setLocation(0, 0);
        playerMob.controls.setLocation(0, 0);

        finishMob.position.setLocation(0, map.mapContent.finishLineV * Config.TILE_HEIGHT);

        for (int i = 0; i < finishLineSegments.size(); i++) {
            MovingObject segment = finishLineSegments.get(i);
            segment.position.setLocation(i * segment.sprite.getImage().getWidth(), finishMob.position.y);
        }

        map.surface.setPosition(0, -map.mapContent.getHeight() * Config.TILE_HEIGHT + Config.getPixelHeight());

//        Logger.getGlobal().log(Level.INFO, "Mobs: {0}", new Object[]{mobs.getAll()});
//        Logger.getGlobal().log(Level.INFO, "Pickups: {0}", new Object[]{temporaryCharacters});
    }

    public MapMaterial getPlayerTile() {
        int u = (int) Math.floor(playerMob.position.x / Config.TILE_WIDTH);
        int v = (int) Math.floor(playerMob.position.y / Config.TILE_HEIGHT);

        return map.mapContent.getItemAt(u, v);
    }

    public MapBoundary.TileType getPlayerBoundaryType() {
        int u = (int) Math.floor(playerMob.position.x / Config.TILE_WIDTH);
        int v = (int) Math.floor(playerMob.position.y / Config.TILE_HEIGHT);
        return map.mapContent.getBoundaryTypeAt(u, v);
    }

    private void evaluatePlayerPosition(double elapsedSeconds) {
        MapMaterial tile = getPlayerTile();
        double speedFactor = getSpeedFactor(tile);
        double f = speedFactor < playerMob.movementSpeedFactor ? 1 * elapsedSeconds : 1;
        double inf = 1 - f;
        playerMob.movementSpeedFactor = playerMob.movementSpeedFactor * inf + speedFactor * f;
    }

    private double getSpeedFactor(MapMaterial tile) {
        if (map.mapContent.GRASS == tile) {
            return 0.6;
        }
        if (map.mapContent.WATER == tile) {
            return 0.3;
        }
        return 1;
    }

    public void update(double elapsedSeconds) {
        evaluatePlayerPosition(elapsedSeconds);

        playerMob.applyControls(elapsedSeconds);

        mobs.beforeCollisionChecking(elapsedSeconds);

        mobs.checkMobCollisions((mob, collisions) -> {
//            Logger.getGlobal().log(Level.INFO, "Mob collisions of {0} : {1}", new Object[]{mob.id, collisions});
            if (Objects.equals(mob.id, playerMob.id)) {
                collisions.forEach((collisionMobId) -> {
                    GameEventQueue.category(PLAY_QUEUE).sendEvent(new PlayerCollisionWithObject(collisionMobId));
                });
            } else {
                collisions.forEach((collisionMobId) -> {
                    if (Objects.equals(collisionMobId, playerMob.id)) {
                        GameEventQueue.category(PLAY_QUEUE).sendEvent(new PlayerCollisionWithObject(mob.id));
                    }
                });
            }
        });

        mobs.afterCollisionChecking();

        map.surface.move(0, -playerMob.movement.y);

        updateAnimations(elapsedSeconds);
    }

    private void updateAnimations(double elapsedSeconds) {
        map.surface.update(elapsedSeconds);

        List<Integer> removeList = new LinkedList<>();

        mobs.getAll().forEach((mob) -> {
            if (mob instanceof Character) {
                Character cmob = (Character) mob;
                if (!cmob.update(elapsedSeconds)) {
                    removeList.add(mob.id);
                    Logger.getGlobal().log(Level.INFO, "remove: {0}", mob.id);
                }
            } else if (mob.sprite != null) {
                mob.sprite.update(elapsedSeconds);
            }
        });

        removeList.forEach((mobId) -> mobs.remove(mobId));
        removeList.clear();
    }

    @OnGameEvent
    public void onPlayerCollisionEvent(PlayerCollisionWithObject event) {
        MovingObject mob = mobs.get(event.objectId);
        if (mob == null) {
            // it's already gone.
            return;
        }

        Character character = mob instanceof Character ? (Character) mob : null;

        if (character != null) {
            switch (character.type) {
                case FINISH:
                    finished = true;
                    break;
                case PICKUP:
                    pickedUpCount++;
                    character.collidesWithMobs = false;
                    character.setState(CharacterState.ACTIVATED);
                    break;
                case SURPRISE:
                    if (playerMob.isControlledState()) {
                        playerMob.setState(CharacterState.STUNNED);
                        playerMob.velocity.y = Config.TILE_HEIGHT * 5;
                        playerMob.velocity.x = playerMob.position.x - character.position.x;
                        if (character.hasStateTransition(CharacterState.ACTIVATING)) {
                            character.setState(CharacterState.ACTIVATING);
                        } else {
                            character.setState(CharacterState.ACTIVATED);
                        }
                    }
                    break;
            }
        }
    }

}
