/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

/**
 *
 * @author mush
 */
public enum CharacterType {
    UNKNOWN,
    PLAYER,
    FINISH,
    PICKUP,
    SURPRISE
}
