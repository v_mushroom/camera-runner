/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import java.awt.Point;

/**
 *
 * @author mush
 */
public class MapPoi {

    public enum Type {
        MAIN_ROAD,
        SIDE_ROAD,
        WATER,
        OFF_ROAD
    }

    public Point point;
    public Type type;

    public MapPoi(Type type, int u, int v) {
        this.type = type;
        this.point = new Point(u, v);
    }

}
