/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import com.mush.game.utils.map.MapContentComparator;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.material.MapMaterials;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author mush
 */
public class MapMaterialComparator extends MapContentComparator<MapMaterial> {

    private Map<MapMaterial, Set<MapMaterial>> ignoreMap;

    private static final String IGNORE_BOUNDARY = "ignoreBoundary";

    public MapMaterialComparator(MapMaterials mapMaterials) {
        ignoreMap = new HashMap<>();
        Set<String> names = mapMaterials.getMaterialNames();
        names.forEach((name) -> {
            MapMaterial material = mapMaterials.getMaterialByName(name);
            if (material.attributes != null && material.attributes.containsKey(IGNORE_BOUNDARY)) {
                ignoreBoundaries(material, mapMaterials);
            }
        });
    }

    @Override
    public boolean isSame(MapMaterial here, MapMaterial there) {
        Set<MapMaterial> ignoreMaterials = ignoreMap.get(here);
        if (ignoreMaterials != null && ignoreMaterials.contains(there)) {
            return true;
        }
        return super.isSame(here, there);
    }

    private void ignoreBoundaries(MapMaterial material, MapMaterials mapMaterials) {
        List<String> ignoreNames = (List<String>) material.attributes.get(IGNORE_BOUNDARY);
        Set<MapMaterial> ignoreMaterials = new HashSet<>();
        
        ignoreNames.forEach((name) -> {
            MapMaterial ignoreMaterial = mapMaterials.getMaterialByName(name);
            if (ignoreMaterial != null) {
                ignoreMaterials.add(ignoreMaterial);
            }
        });
        
        ignoreMap.put(material, ignoreMaterials);
    }

}
