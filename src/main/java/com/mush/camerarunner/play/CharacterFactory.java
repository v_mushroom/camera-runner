/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.play;

import com.mush.camerarunner.Config;
import com.mush.game.utils.sprites.SpriteFactory;
import java.awt.image.BufferedImage;

/**
 *
 * @author mush
 */
public class CharacterFactory {

    private SpriteFactory spriteFactory;

    public CharacterFactory(SpriteFactory spriteFactory) {
        this.spriteFactory = spriteFactory;
    }

    public Character potato() {
        Character character = new Character(spriteFactory.createSprite("POTATO"), CharacterType.PICKUP);
        character.setSpriteStateName(CharacterState.ACTIVATED, CharacterState.SPRITE_STATE_ACTIVATED);
        character.collidesWithMap = false;
        character.isObstacleForMobs = false;

        BufferedImage image = character.sprite.getImage();
        character.bounds.set(image.getWidth() / 2, Config.TILE_HEIGHT / 4, image.getWidth() / 2, 0);
        return character;
    }

    public Character mushroom() {
        Character character = new Character(spriteFactory.createSprite("MUSHROOM"), CharacterType.SURPRISE);
        character.setSpriteStateName(CharacterState.ACTIVATED, CharacterState.SPRITE_STATE_ACTIVATED);

        character.addStateTransition(CharacterState.ACTIVATED, CharacterState.IDLE);

        character.collidesWithMap = false;
        character.isObstacleForMobs = false;
        character.bounds.set(character.sprite.getImage().getWidth() / 4, Config.TILE_HEIGHT / 4, character.sprite.getImage().getWidth() / 4, 0);
        return character;
    }

    public Character shark() {
        Character character = new Character(spriteFactory.createSprite("SHARK"), CharacterType.SURPRISE);
        character.setSpriteStateName(CharacterState.ACTIVATED, CharacterState.SPRITE_STATE_ACTIVATED);
        character.setSpriteStateName(CharacterState.ACTIVATING, CharacterState.SPRITE_STATE_ACTIVATING);
        character.setSpriteStateName(CharacterState.ACTIVATING2, CharacterState.SPRITE_STATE_ACTIVATING2);
        character.setSpriteStateName(CharacterState.DEACTIVATING, CharacterState.SPRITE_STATE_DEACTIVATING);

        character.addStateTransition(CharacterState.ACTIVATING, CharacterState.ACTIVATING2);
        character.addStateTransition(CharacterState.ACTIVATING2, CharacterState.ACTIVATED);
        character.addStateTransition(CharacterState.ACTIVATED, CharacterState.DEACTIVATING);
        character.addStateTransition(CharacterState.DEACTIVATING, CharacterState.IDLE);

        character.collidesWithMap = false;
        character.isObstacleForMobs = false;
        character.bounds.set(character.sprite.getImage().getWidth() / 4, Config.TILE_HEIGHT / 4, character.sprite.getImage().getWidth() / 4, 0);
        return character;
    }

}
