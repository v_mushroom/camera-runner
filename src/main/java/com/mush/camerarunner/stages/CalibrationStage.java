/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.stages;

import com.mush.camerarunner.CameraRunner;
import com.mush.camerarunner.camera.CameraConnection;
import com.mush.camerarunner.camera.PlayerTracker;
import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.render.Renderable;
import com.mush.game.utils.swing.GameKeyListener;
import com.mush.game.utils.ui.UiButton;
import com.mush.game.utils.ui.UiButtonList;
import com.mush.game.utils.ui.UiButtonRenderer;
import java.awt.event.KeyEvent;

/**
 *
 * @author mush
 */
public class CalibrationStage extends Stage {

    public static enum CalibrationAction {
        COLOR,
        RANGE,
        ACTIVATE
    }

    private final PlayerTracker playerTracker;
    private final CalibrationView view;
    public final UiButtonList buttonList;
    private final UiButtonRenderer buttonRenderer;
    private final CameraConnection cameraConnection;

    private UiButton hueButton;
    private UiButton satButton;
    private UiButton valButton;
    private UiButton totalButton;

    public CalibrationStage(GameKeyListener keyListener, PlayerTracker playerTracker, CameraConnection cameraConnection) {
        super(keyListener);
        this.playerTracker = playerTracker;
        this.cameraConnection = cameraConnection;

        getKeyboard().bindActionKey(KeyEvent.VK_ESCAPE, KeyEvent.VK_ESCAPE);

        getKeyboard().bindActionKey(KeyEvent.VK_ENTER, KeyEvent.VK_ENTER);
        getKeyboard().bindActionKey(KeyEvent.VK_UP, KeyEvent.VK_UP);
        getKeyboard().bindActionKey(KeyEvent.VK_DOWN, KeyEvent.VK_DOWN);
        getKeyboard().bindActionKey(KeyEvent.VK_LEFT, KeyEvent.VK_LEFT);
        getKeyboard().bindActionKey(KeyEvent.VK_RIGHT, KeyEvent.VK_RIGHT);

        buttonList = new UiButtonList();
        buttonRenderer = new UiButtonRenderer();

        setupButtons();

        view = new CalibrationView(this, playerTracker);
    }

    private void setupButtons() {
        int width = 50;
        int height = 15;

        buttonList.location.x = 5;
        buttonList.location.y = 30;
        buttonList.loopSelection = true;
        buttonList.direction.setLocation(0, 1);
        buttonList.spacing = 2;

        UiButton button;

        button = new UiButton("Color", buttonRenderer);
        button.setSize(width, height);
        button.action = CalibrationAction.COLOR;
        buttonList.list.add(button);
/*
        button = new UiButton("Left", buttonRenderer);
        button.setSize(width, height);
        button.action = CalibrationAction.RANGE;
        buttonList.list.add(button);

        button = new UiButton("Right", buttonRenderer);
        button.setSize(width, height);
        button.action = CalibrationAction.RANGE;
        buttonList.list.add(button);
*/
        button = new UiButton("Hue", buttonRenderer);
        button.setSize(width, height);
        buttonList.list.add(button);
        hueButton = button;

        button = new UiButton("Sat", buttonRenderer);
        button.setSize(width, height);
        buttonList.list.add(button);
        satButton = button;

        button = new UiButton("Val", buttonRenderer);
        button.setSize(width, height);
        buttonList.list.add(button);
        valButton = button;

        button = new UiButton("Total", buttonRenderer);
        button.setSize(width, height);
        buttonList.list.add(button);
        totalButton = button;

        button = new UiButton("Activate", buttonRenderer);
        button.setSize(width, height);
        button.action = CalibrationAction.ACTIVATE;
        buttonList.list.add(button);

        button = new UiButton("Back", buttonRenderer);
        button.setSize(width, height);
        button.action = new CameraRunner.ChangeStageEvent(CameraRunner.StageState.HOME);
        buttonList.list.add(button);

        buttonList.layout();
        buttonList.select(0);
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof Integer) {
            switch ((int) action.message) {
                case KeyEvent.VK_ENTER:
                    UiButton button = buttonList.getSelected();
                    if (button != null && button.action != null) {
                        onButtonAction(button.action);
                    }
                    break;
                case KeyEvent.VK_UP:
                    buttonList.selectPrevious();
                    break;
                case KeyEvent.VK_DOWN:
                    buttonList.selectNext();
                    break;

                case KeyEvent.VK_LEFT:
                    if (buttonList.getSelected() == hueButton) {
                        playerTracker.finder.colorFinder.hueTreshold -= 0.05;
                    }
                    if (buttonList.getSelected() == satButton) {
                        playerTracker.finder.colorFinder.satTreshold -= 0.05;
                    }
                    if (buttonList.getSelected() == valButton) {
                        playerTracker.finder.colorFinder.valTreshold -= 0.05;
                    }
                    if (buttonList.getSelected() == totalButton) {
                        playerTracker.finder.colorFinder.totalTreshold -= 0.05;
                    }
                    limitTresholds();
                    break;
                case KeyEvent.VK_RIGHT:
                    if (buttonList.getSelected() == hueButton) {
                        playerTracker.finder.colorFinder.hueTreshold += 0.05;
                    }
                    if (buttonList.getSelected() == satButton) {
                        playerTracker.finder.colorFinder.satTreshold += 0.05;
                    }
                    if (buttonList.getSelected() == valButton) {
                        playerTracker.finder.colorFinder.valTreshold += 0.05;
                    }
                    if (buttonList.getSelected() == totalButton) {
                        playerTracker.finder.colorFinder.totalTreshold += 0.05;
                    }
                    limitTresholds();
                    break;

                case KeyEvent.VK_ESCAPE:
                    GameEventQueue
                            .category(GameEventQueue.Categories.NAVIGATION)
                            .sendEvent(new CameraRunner.ChangeStageEvent(CameraRunner.StageState.HOME));
                    break;
            }
        }
    }

    private void limitTresholds() {
        playerTracker.finder.colorFinder.hueTreshold = Math.min(1, Math.max(0, playerTracker.finder.colorFinder.hueTreshold));
        playerTracker.finder.colorFinder.satTreshold = Math.min(1, Math.max(0, playerTracker.finder.colorFinder.satTreshold));
        playerTracker.finder.colorFinder.valTreshold = Math.min(1, Math.max(0, playerTracker.finder.colorFinder.valTreshold));
        playerTracker.finder.colorFinder.totalTreshold = Math.min(1, Math.max(0, playerTracker.finder.colorFinder.totalTreshold));
    }

    private void onButtonAction(Object action) {
        if (action instanceof CameraRunner.ChangeStageEvent) {
            GameEventQueue
                    .category(GameEventQueue.Categories.NAVIGATION)
                    .sendEvent((CameraRunner.ChangeStageEvent) action);

        } else if (action instanceof CalibrationAction) {
            switch ((CalibrationAction) action) {
                case COLOR:
                    playerTracker.setCalibratingColor(true);
                    break;
                case RANGE:
                    break;
                case ACTIVATE:
                    cameraConnection.setActive(!cameraConnection.getActive());
                    break;
            }
        }
    }

    @Override
    public void update(double elapsedSeconds) {

    }

    @Override
    public void cleanup() {
        playerTracker.setCalibratingColor(false);
    }

    @Override
    public Renderable getView() {
        return view;
    }

}
