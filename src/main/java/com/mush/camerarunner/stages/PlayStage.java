/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.stages;

import com.mush.camerarunner.CameraRunner;
import com.mush.camerarunner.Config;
import com.mush.camerarunner.camera.PlayerTrackedUiChoice;
import com.mush.camerarunner.camera.PlayerTracker;
import com.mush.camerarunner.play.PlayLevel;
import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.render.Renderable;
import com.mush.game.utils.sprites.SpriteFactory;
import com.mush.game.utils.swing.GameKeyListener;
import java.awt.event.KeyEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mush
 */
public class PlayStage extends Stage {

    public enum PlayEventType {
        RESET
    };

    public static class PlayStateEvent {

        public final PlayEventType type;

        public PlayStateEvent(PlayEventType type) {
            this.type = type;
        }
    }

    public static final String PLAY_QUEUE = "PLAY";
    public final PlayerTracker playerTracker;

    public PlayLevel level;

    public double totalTime;
    public double stepCount;

    private final PlayView view;
    private final SpriteFactory spriteFactory;

    private double manualUpDown;
    private double manualLeftRight;
    private double manualScrollUpDown;
    private boolean trackerEnabled = true;
    public boolean showDebug = false;

    public PlayerTrackedUiChoice uiChoice;

    public PlayStage(GameKeyListener keyListener, PlayerTracker playerTracker) {
        super(keyListener);

        GameEventQueue.category(GameEventQueue.Categories.NAVIGATION).addEventListener(this);
        GameEventQueue.category(PLAY_QUEUE).addEventListener(this);

        this.playerTracker = playerTracker;

        getKeyboard().bindActionKey(KeyEvent.VK_ESCAPE, KeyEvent.VK_ESCAPE);
        getKeyboard().bindActionKey(KeyEvent.VK_E, KeyEvent.VK_E);
        getKeyboard().bindActionKey(KeyEvent.VK_D, KeyEvent.VK_D);
        getKeyboard().bindActionKey(KeyEvent.VK_R, KeyEvent.VK_R);

        getKeyboard().bindStateKey(KeyEvent.VK_LEFT, KeyEvent.VK_LEFT);
        getKeyboard().bindStateKey(KeyEvent.VK_RIGHT, KeyEvent.VK_RIGHT);
        getKeyboard().bindStateKey(KeyEvent.VK_UP, KeyEvent.VK_UP);
        getKeyboard().bindStateKey(KeyEvent.VK_DOWN, KeyEvent.VK_DOWN);
        getKeyboard().bindStateKey(KeyEvent.VK_PAGE_UP, KeyEvent.VK_PAGE_UP);
        getKeyboard().bindStateKey(KeyEvent.VK_PAGE_DOWN, KeyEvent.VK_PAGE_DOWN);

        spriteFactory = new SpriteFactory();
        try {
            spriteFactory.load("res/sprites.json");
        } catch (Exception e) {
            Logger.getGlobal().log(Level.SEVERE, "", e);
        }

        uiChoice = new PlayerTrackedUiChoice(playerTracker);
        uiChoice.addChoice("Back", new CameraRunner.ChangeStageEvent(CameraRunner.StageState.HOME), -0.5, 0.5);
        uiChoice.addChoice("Continue", new PlayStateEvent(PlayEventType.RESET), 0.5, 0.5);

        level = new PlayLevel(spriteFactory);

        reset();

        view = new PlayView(this);
    }

    public void reset() {
        level.reset();

        totalTime = 0;
        stepCount = 0;

        uiChoice.reset();
        playerTracker.reset();
    }

    @OnGameEvent
    public void onGameEvent(PlayStateEvent playEvent) {
        switch (playEvent.type) {
            case RESET:
                reset();
                break;
        }
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof Integer) {
            switch ((int) action.message) {

                case KeyEvent.VK_ESCAPE:
                    GameEventQueue
                            .category(GameEventQueue.Categories.NAVIGATION)
                            .sendEvent(new CameraRunner.ChangeStageEvent(CameraRunner.StageState.HOME));
                    break;
                case KeyEvent.VK_E:
                    trackerEnabled = !trackerEnabled;
                    break;
                case KeyEvent.VK_D:
                    showDebug = !showDebug;
                    break;
                case KeyEvent.VK_R:
                    GameEventQueue
                            .category(GameEventQueue.Categories.NAVIGATION)
                            .sendEvent(new PlayStateEvent(PlayEventType.RESET));
                    break;
            }
        }
    }

    @OnGameEvent
    public void onAction(GameInputEvent.State state) {
        if (state.message instanceof Integer) {
            switch ((int) state.message) {
                case KeyEvent.VK_LEFT:
                    manualLeftRight = state.active ? -1 : 0;
                    break;
                case KeyEvent.VK_RIGHT:
                    manualLeftRight = state.active ? 1 : 0;
                    break;
                case KeyEvent.VK_UP:
//                    manualUpDown = state.active ? -1 : 0;
                    manualUpDown += state.active ? -0.25 : 0.0;
                    break;
                case KeyEvent.VK_DOWN:
//                    manualUpDown = state.active ? 1 : 0;
                    manualUpDown += state.active ? 0.25 : -0.0;
                    break;
                case KeyEvent.VK_PAGE_UP:
                    manualScrollUpDown = state.active ? -1 : 0;
                    break;
                case KeyEvent.VK_PAGE_DOWN:
                    manualScrollUpDown = state.active ? 1 : 0;
                    break;
            }
        }
    }

    @Override
    public Renderable getView() {
        return view;
    }

    @Override
    public void cleanup() {
        GameEventQueue.category(GameEventQueue.Categories.NAVIGATION).removeEventListener(this);
        GameEventQueue.category(PLAY_QUEUE).removeEventListener(this);
    }

    private void updatePlayerControlsFromTracker() {
        level.playerMob.controls.y = -playerTracker.getJogFrequency();

        if (Math.abs(level.playerMob.controls.y) < 0.1) {
            level.playerMob.controls.y = 0;
        }

        double trackerOfs = Math.abs(playerTracker.getPlayerPosition());
        double playerOfs = Math.abs(level.playerMob.controls.x);

        if (playerOfs == 0) {
            if (trackerOfs > 0.4) {
                level.playerMob.controls.x = -Math.signum(playerTracker.getPlayerPosition());
            }
        } else {
            if (trackerOfs < 0.3) {
                level.playerMob.controls.x = 0;
            }
        }
    }

    private void updatePlayerControlsManually(double elapsedSeconds) {
        level.playerMob.controls.x += manualLeftRight;
        level.playerMob.controls.y += manualUpDown * 3;                
        level.map.surface.move(0, manualScrollUpDown * elapsedSeconds * Config.TILE_HEIGHT * 15);
    }

    @Override
    public void update(double elapsedSeconds) {
        if (level.finished) {
            uiChoice.update(elapsedSeconds);
        }

        level.playerMob.controls.setLocation(0, 0);

        if (!level.finished) {
            updatePlayerControls(elapsedSeconds);
        }

        if (!level.finished) {
            level.update(elapsedSeconds);
        }
    }

    private void updatePlayerControls(double elapsedSeconds) {
        totalTime += elapsedSeconds;

        if (trackerEnabled) {
            updatePlayerControlsFromTracker();

            stepCount += playerTracker.getJogFrequency() * elapsedSeconds;
        } else {
            level.playerMob.controls.setLocation(0, 0);
        }

        updatePlayerControlsManually(elapsedSeconds);

        level.playerMob.controls.x *= Config.TILE_WIDTH * 2;
        level.playerMob.controls.y *= Config.TILE_HEIGHT;
    }

}
