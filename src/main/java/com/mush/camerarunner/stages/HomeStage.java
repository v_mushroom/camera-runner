/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.stages;

import com.mush.camerarunner.CameraRunner;
import com.mush.camerarunner.Config;
import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameInputEvent;
import com.mush.game.utils.core.OnGameEvent;
import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.render.Renderable;
import com.mush.game.utils.swing.GameKeyListener;
import com.mush.game.utils.ui.UiButton;
import com.mush.game.utils.ui.UiButtonList;
import com.mush.game.utils.ui.UiButtonRenderer;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

/**
 *
 * @author mush
 */
public class HomeStage extends Stage implements Renderable {

    private final UiButtonList buttonList;
    private final UiButtonRenderer buttonRenderer;

    public HomeStage(GameKeyListener keyListener) {
        super(keyListener);

        getKeyboard().bindActionKey(KeyEvent.VK_ENTER, KeyEvent.VK_ENTER);
        getKeyboard().bindActionKey(KeyEvent.VK_UP, KeyEvent.VK_UP);
        getKeyboard().bindActionKey(KeyEvent.VK_DOWN, KeyEvent.VK_DOWN);

        buttonList = new UiButtonList();
        buttonRenderer = new UiButtonRenderer();

        setupButtons();
    }

    private void setupButtons() {
        int width = 70;
        int height = 15;

        buttonList.location.x = Config.getPixelWidth() / 2 - width / 2;
        buttonList.location.y = 30;
        buttonList.loopSelection = true;
        buttonList.direction.setLocation(0, 1);
        buttonList.spacing = 2;

        UiButton button;

        button = new UiButton("Calibrate", buttonRenderer);
        button.setSize(width, height);
        button.action = new CameraRunner.ChangeStageEvent(CameraRunner.StageState.CALIBRATION);
        buttonList.list.add(button);

        button = new UiButton("Play", buttonRenderer);
        button.setSize(width, height);
        button.action = new CameraRunner.ChangeStageEvent(CameraRunner.StageState.PLAY);
        buttonList.list.add(button);

        button = new UiButton("Quit", buttonRenderer);
        button.setSize(width, height);
        button.action = new CameraRunner.ChangeStageEvent(CameraRunner.StageState.QUIT);
        buttonList.list.add(button);

        buttonList.layout();
        buttonList.select(0);
    }

    @Override
    public Renderable getView() {
        return this;
    }

    @OnGameEvent
    public void onAction(GameInputEvent.Action action) {
        if (action.message instanceof Integer) {
            switch ((Integer) action.message) {
                case KeyEvent.VK_ENTER:
                    UiButton button = buttonList.getSelected();
                    if (button != null && button.action != null) {
                        GameEventQueue
                                .category(GameEventQueue.Categories.NAVIGATION)
                                .sendEvent(button.action);
                    }
                    break;
                case KeyEvent.VK_UP:
                    buttonList.selectPrevious();
                    break;
                case KeyEvent.VK_DOWN:
                    buttonList.selectNext();
                    break;
            }
        }
    }

    @Override
    public void update(double elapsedSeconds) {

    }

    @Override
    public void renderContent(Graphics2D g, GameRenderer renderer) {
        g.setColor(Color.WHITE);
        g.drawString("Camera Runner", 5, 20);

        buttonList.draw(g);
    }

    @Override
    public void cleanup() {
    }

}
