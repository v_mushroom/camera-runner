/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.stages;

import com.mush.game.utils.core.GameEventQueue;
import com.mush.game.utils.core.GameKeyboard;
import com.mush.game.utils.core.Updateable;
import com.mush.game.utils.render.Renderable;
import com.mush.game.utils.swing.GameKeyListener;

/**
 *
 * @author mush
 */
public abstract class Stage implements Updateable {

    private final GameKeyListener keyListener;
    private final GameKeyboard keyboard;

    public Stage(GameKeyListener keyListener) {
        this.keyListener = keyListener;

        keyboard = new GameKeyboard(GameEventQueue.Categories.INPUT);
        keyListener.addKeyboard(keyboard);

        GameEventQueue.category(GameEventQueue.Categories.INPUT).addEventListener(this);
    }

    public final GameKeyboard getKeyboard() {
        return keyboard;
    }

    @Override
    public void updateCurrentFps(double fps) {
    }

    public void stop() {
        keyListener.removeKeyboard(keyboard);
        GameEventQueue.category(GameEventQueue.Categories.INPUT).removeEventListener(this);

        cleanup();
    }

    public abstract Renderable getView();

    public abstract void cleanup();

}
