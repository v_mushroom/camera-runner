/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.stages;

import com.mush.camerarunner.Config;
import com.mush.camerarunner.camera.PlayerTracker;
import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.render.Renderable;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author mush
 */
public class CalibrationView implements Renderable {

    private final PlayerTracker playerTracker;
    private final CalibrationStage stage;

    public CalibrationView(CalibrationStage stage, PlayerTracker playerTracker) {
        this.playerTracker = playerTracker;
        this.stage = stage;
    }

    @Override
    public void renderContent(Graphics2D g, GameRenderer renderer) {
        g.setColor(Color.WHITE);
        g.drawString("Calibration", 5, 20);

        renderColorSamples(g, renderer);

        stage.buttonList.draw(g);
    }

    private void renderColorSamples(Graphics2D g, GameRenderer renderer) {
//        int screenWidth = renderer.getScreenImage().getWidth();
//        int screenHeight = renderer.getScreenImage().getHeight();
        int screenWidth = Config.getPixelWidth();
        int screenHeight = Config.getPixelHeight();
//        int centerX = screenWidth / 2;
//        int centerY = screenHeight / 2;
        int centerX = 80;
        int centerY = 50;
        int width = PlayerTracker.CALIBRATION_WIDTH;
        int height = PlayerTracker.CALIBRATION_HEIGHT;
        int hWidth = width / 2;
        int hHeight = height / 2;
        int boxSize = 4;
        int boxStep = boxSize + 1;
        int halfBoxSize = boxSize / 2;
        int boxesWidth = (width - 1) * boxStep + boxSize + 2;
        int boxesHeight = (height - 1) * boxStep + boxSize + 2;

        g.setColor(Color.DARK_GRAY);
        g.fillRect(
                centerX - hWidth * boxStep - halfBoxSize - 1,
                centerY - hHeight * boxStep - halfBoxSize - 1,
                (width - 1) * boxStep + boxSize + 2,
                (height - 1) * boxStep + boxSize + 2);

        if (playerTracker.isCalibratingColor()) {
            g.setColor(Color.LIGHT_GRAY);
            g.drawRect(
                    centerX - boxStep - halfBoxSize - 1,
                    centerY - boxStep - halfBoxSize - 1,
                    2 * boxStep + boxSize + 1,
                    2 * boxStep + boxSize + 1);
        }

        g.setColor(Color.GRAY);
        Color[] centerColors = playerTracker.getCenterColors();
        for (int i = 0; i < centerColors.length; i++) {
            int u = i % width;
            int v = i / width;
            g.setColor(centerColors[i]);
            g.fillRect(
                    centerX + (u - hWidth) * boxStep - halfBoxSize,
                    centerY + (v - hHeight) * boxStep - halfBoxSize,
                    4, 4);
        }

        if (playerTracker.isCalibratingColor()) {
            g.setColor(Color.DARK_GRAY);
        } else {
            g.setColor(Color.LIGHT_GRAY);
        }
        g.drawRect(centerX - 6, centerY - 20 - hHeight * boxStep - 1, 11, 11);

        g.setColor(playerTracker.getTrackedColor());
        g.fillRect(centerX - 5, centerY - 20 - hHeight * boxStep, 10, 10);

        drawPercent(centerX + 20, centerY - 15, 20, playerTracker.finder.colorFinder.hueTreshold, g);
        drawPercent(centerX + 25, centerY - 15, 20, playerTracker.finder.colorFinder.satTreshold, g);
        drawPercent(centerX + 30, centerY - 15, 20, playerTracker.finder.colorFinder.valTreshold, g);

        drawPercent(centerX + 40, centerY - 15, 20, playerTracker.finder.colorFinder.totalTreshold, g);
    }

    private void drawPercent(int x, int y, int height, float v, Graphics2D g) {
        g.setColor(Color.GRAY);
        g.fillRect(x, y, 1, height);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x - 1, y + (int) (height * (1 - v)), 3, 1);
    }

    public void showImage(BufferedImage cameraImage, Graphics2D g, GameRenderer renderer) {
        if (cameraImage == null) {
            return;
        }

        int screenWidth = renderer.getScreenImage().getWidth();
        int screenHeight = renderer.getScreenImage().getHeight();
        int width = PlayerTracker.CALIBRATION_WIDTH;
        int height = PlayerTracker.CALIBRATION_HEIGHT;
        int boxSize = 4;
        int boxStep = boxSize + 1;
        int boxesWidth = (width - 1) * boxStep + boxSize + 2;
        int boxesHeight = (height - 1) * boxStep + boxSize + 2;

        double scale = (double) boxesWidth / cameraImage.getWidth();

        AffineTransform t = g.getTransform();

//        int x = screenWidth / 2 - boxesWidth / 2;
//        int y = screenHeight / 2 + boxesHeight / 2 + 1;
//        int x = Config.getPixelWidth() / 2 - boxesWidth / 2;
//        int y = Config.getPixelHeight() / 2 + boxesHeight / 2 + 1;
        int x = 80 - boxesWidth / 2;
        int y = 50 + boxesHeight;

        g.translate(x, y);
        g.scale(scale, scale);

        g.drawImage(cameraImage, null, 0, 0);

        g.setTransform(t);

        int imageW = (int) (cameraImage.getWidth() * scale) - 1;
        int imageH = (int) (y + cameraImage.getHeight() * scale);

        g.translate(x, imageH + 5);
        try {
            int refStep = 1;
            playerTracker.finder.colorFinder.drawReferenceImage(cameraImage, refStep, g);

            if (playerTracker.foundPatch != null) {
                Rectangle2D patch = playerTracker.foundPatch;
                g.setColor(Color.GREEN);
                int refWidth = cameraImage.getWidth() / refStep;
                int refHeight = cameraImage.getHeight() / refStep;
                g.drawRect(
                        (int) (patch.getX() * refWidth),
                        (int) (patch.getY() * refHeight),
                        (int) (patch.getWidth() * refWidth),
                        (int) (patch.getHeight() * refHeight)
                );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        g.setTransform(t);

        g.setColor(Color.GRAY);
        g.fillRect(x, imageH + 2, imageW + 1, 1);

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x + (int) (playerTracker.getLeftRangeBound() * imageW), imageH + 1, 1, 3);
        g.fillRect(x + (int) (playerTracker.getRightRangeBound() * imageW), imageH + 1, 1, 3);
    }

}
