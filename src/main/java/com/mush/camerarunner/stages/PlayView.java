/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mush.camerarunner.stages;

import com.mush.camerarunner.Config;
import com.mush.game.utils.map.MapBoundary;
import com.mush.game.utils.map.material.MapMaterial;
import com.mush.game.utils.map.move.Box;
import com.mush.game.utils.map.move.MovingObject;
import com.mush.game.utils.render.GameRenderer;
import com.mush.game.utils.render.Renderable;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author mush
 */
public class PlayView implements Renderable {

    private final PlayStage stage;

    public PlayView(PlayStage stage) {
        this.stage = stage;
    }

    @Override
    public void renderContent(Graphics2D g, GameRenderer renderer) {
        int screenWidth = Config.getPixelWidth();
        int screenHeight = Config.getPixelHeight();

        stage.level.map.surface.draw(g);

        drawMobs(g);
        if (!stage.level.finished) {
            if (stage.playerTracker.lastImage != null) {
                AffineTransform t = g.getTransform();
                g.translate(screenWidth - 33, 1);
                stage.playerTracker.finder.colorFinder.drawReferenceImage(stage.playerTracker.lastImage, 5, g);
                g.setTransform(t);
            }
        }
        
        if (stage.level.finished) {
            AffineTransform t = g.getTransform();
            g.translate((screenWidth - 150) / 2, (screenHeight - 100) / 2);
            g.setColor(Color.DARK_GRAY);
            g.fillRect(0, 0, 150, 100);
            g.setColor(Color.LIGHT_GRAY);

            Duration duration = Duration.ofMillis((long) (stage.totalTime * 1000));

            g.drawString("A winner is you!", 5, 15);
            g.drawString("Time : " + formatDuration(duration), 5, 25);
            g.drawString("Steps: " + (int) (stage.stepCount), 5, 35);
            g.drawString("Potatoes: " + stage.level.pickedUpCount, 5, 45);

            g.setTransform(t);
            
            stage.uiChoice.draw(g);
        }

        if (stage.showDebug) {
            diag(g);
        }
    }

    private void drawMobs(Graphics2D g) {
        AffineTransform t = g.getTransform();
        g.translate(0, (int)stage.level.map.surface.getPosition().y);

        List<MovingObject> mobs = new ArrayList<>();
        stage.level.mobs.getAll().forEach((mob) -> {
            // if
            mobs.add(mob);
        });
        
        Collections.sort(mobs, new Comparator<MovingObject>() {
            @Override
            public int compare(MovingObject o1, MovingObject o2) {
                int p1 = (int) (o1.position.y + o1.bounds.bottom);
                int p2 = (int) (o2.position.y + o2.bounds.bottom);
                return p1 - p2;
            }
        });
        

        mobs.forEach((mob) -> {
            g.setColor(Color.RED);
            if (mob.sprite != null) {
                mob.sprite.draw(g, (int) mob.position.getX(), (int) mob.position.getY());
            } else {
//                drawBox(g, mob);
            }
            g.setColor(Color.MAGENTA);
//            drawBox(g, mob);
//            g.setColor(Color.BLUE);
//            drawBox(g, mob.movingBox);
//            g.fillRect((int) mob.position.getX(), (int) mob.position.getY(), 1, 1);
//            g.drawRect((int) (mob.position.getX() + mob.bounds.left), (int) (mob.position.getY() + mob.bounds.top), 
//                    (int)(mob.bounds.right - mob.bounds.left - 1), 
//                    (int)(mob.bounds.bottom - mob.bounds.top - 1));
        });
        
        if (!stage.level.finished) {
            g.setColor(Color.YELLOW);
//            g.fillRect((int) stage.trackerPositionPositionX(), (int) (stage.playerMob.position.y + 2), 1, 10);
        }

        g.setTransform(t);
    }
    private void drawBox(Graphics2D g, MovingObject mob) {
        int x = (int) (mob.position.x - mob.bounds.left);
        int y = (int) (mob.position.y - mob.bounds.top);
        int w = (int) (mob.bounds.right + mob.bounds.left - 1);
        int h = (int) (mob.bounds.bottom + mob.bounds.top - 1);

        g.drawRect(x, y, w, h);
        g.fillRect((int) mob.position.x, (int) mob.position.y, 1, 1);
    }

    private void drawBox(Graphics2D g, Box box) {
        int x = (int) box.left;
        int y = (int) box.top;
        int w = (int) (box.right - box.left - 1);
        int h = (int) (box.bottom - box.top - 1);

        g.drawRect(x, y, w, h);
    }

    private String formatDuration(Duration duration) {
        long minutes = duration.toMinutes();
        long seconds = duration.getSeconds() % 60;
        long millis = duration.toMillis() % 1000;
        long hundreds = millis / 10;

        StringBuilder sb = new StringBuilder();
        if (minutes > 0) {
            sb.append(minutes).append(" min ");
        }

        sb.append(seconds).append(".").append(hundreds).append(" sec");

        return sb.toString();
    }

    private void diag(Graphics2D g) {
        g.setColor(Color.YELLOW);
        Point2D.Double player = stage.level.playerMob.position;
        Point2D.Double velocity = stage.level.playerMob.velocity;
        int u = (int) Math.floor(player.x / Config.TILE_WIDTH);
        int v = (int) Math.floor(player.y / Config.TILE_HEIGHT);
//        g.fillRect(0, (int) stage.map.surface.getPosition().y, screenWidth, 1);
        g.drawRect((int) player.x - Config.TILE_WIDTH / 2, (int) (player.y + stage.level.map.surface.getPosition().y) - Config.TILE_HEIGHT / 2, Config.TILE_WIDTH, Config.TILE_HEIGHT);
//        MapMaterial tile = stage.map.mapContent.getItemAt(u, v);
        MapMaterial tile = stage.level.getPlayerTile();
        MapBoundary.TileType type = stage.level.getPlayerBoundaryType();
//        System.out.println(playerPosition);
//        System.out.println(u + " " + v);
//        System.out.println(tile.name);
        g.drawString(tile.name, 10, 50);
        g.drawString(type.name(), 10, 60);

        g.drawString(u + " " + v, 10, 70);
        g.drawString((int) player.x + " " + (int) player.y, 10, 90);
        g.drawString((int) velocity.x + " " + (int) velocity.y, 10, 100);
//        g.drawString("" + stage.playerTracker.getPlayerPosition(), 10, 110);
        g.drawString("" + (int) stage.stepCount, 10, 130);
        g.drawString("" + (int) (stage.level.playerMob.movementSpeedFactor * 10), 10, 140);
//        stage.playerTracker.getPlayerPosition();
    }

}
